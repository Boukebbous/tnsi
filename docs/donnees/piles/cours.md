# Piles : Cours

## Définition

<def>

Une <bred>Pile</bred> :fr: / **Stack** :uk: / **LIFO** :uk: (**Last In First Out** ou **Dernier Arrivé Premier Sorti** ) est une collection d'objets munie des opérations/**primitives** suivantes:
* savoir la pile est vide: `est_vide` ou `is_empty`
* <bred>*empiler*</bred> / <bred>push</bred> un nouvel élément au ***sommet*** de la pile, en temps constant : `empiler` ou `push`
* <bred>*dépiler*</bred> / <bred>pop</bred> l'élément courant au ***sommet*** de la pile, en temps constant : `depiler` ou `pop`
Parfois, on peut trouver deux opérations distinctes:
  * une opération pour (seulement) renvoyer le sommet de la pile, sans le supprimer
  * une opération pour le supprimer
* **Sommet** renvoie la valeur du Sommet

</def>

<exp>

* Pile d'assiettes: c'est en haut de la pile qu'il faut prendre ou ajouter des assiettes.
* Pile de Livres: idem. etc..

</exp>

## Principales Utilisations

### Traitement des appels de fonctions

gestion des adresses de retour, nécessaire dans le cas de fonctions récursives…

### Évaluation d'expressions arithmétiques

les algorithmes utilisent la structure de Pile

## Spécification

<pte data="Préconditions">

* dépiler est défini $\Leftrightarrow$ est_vide=Faux
* Sommet est définie $\Leftrightarrow$ est_vide=Faux

</pte>

<pte  data="Axiomes">

Une Pile dispose des axiomes suivants:
* est_vide(Pile_vide) = Vrai
* est_vide(empiler(Pile p,Element e)) = Faux
* dépiler(empiler(Pile p, Element e)) = p
* Sommet(empiler(Pile p, Element e)) = e

</pte>


## Implémentations

### Avec un Tableau Dynamique (avec le type `list` de Python)

### Avec le module `collections.deque`