# Piles : Introduction

Une **Pile** :fr:, ou **Stack** :uk: est une collection d'objets, ayant des opérations basées sur la structure **LIFO (Last In, First Out)** :uk: / **Dernier Arrivé, Premier Sorti** :fr:.
Dans une pile, **les ajouts et suppressions n'ont lieu que sur une même extremité** : le **Sommet** de la Pile.

<div style="float:left;">

```dot
digraph pile {
  rankdir=LR

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Pile contenant | 4, 8 et 5 ", color=none, width=0.5]
  headerIndices [label="", color=none]
  headerValues -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=12, width=0.5, height = 1.5, fixedsize=true];
  values [label="<f0> 4 | <f2> 8 | <f3> 5 ", color=blue, fillcolor=lightblue, style=filled, width=0.5];
  indices [label="<f0> Sommet |  |  |  |  | ", color=white, fontcolor=red, fontsize=12, width=0.7];

  node [shape=plaintext, fontcolor=red, fontsize=12, height = 0.3];
  pop [label="1", fontcolor=none]
  " " [label=" |  ", shape=Mrecord, height=0.75, fontcolor=none; color=none]
  " " -> pop [color=none, tailclip=false,]

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
}
```

</div>

<div style="float:left;">

```dot
digraph pile {
  rankdir=LR

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Empilement / | Push | de la valeur 2 ", color=none, width=0.5]
  headerIndices [label="", color=none]
  headerValues -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=12, width=0.5, height = 2, fixedsize=true];
  values [label="<f0> 2 | <f1> 4 | <f2> 8 | <f3> 5 ", color=blue, fillcolor=lightblue, style=filled, width=0.5];
  indices [label="<f0> Sommet |  |  |  |  | ", color=white, fontcolor=red, fontsize=12, width=0.7];

  node [shape=plaintext, fontcolor=red, fontsize=12, height = 0.3];
  pop [label="1", fontcolor=none]
  " " [shape=Mrecord,color=none]
  " " -> pop [color=none, tailclip=false,]

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
}
```

</div>

<div style="float:left;">

```dot
digraph pile {
  rankdir=LR

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Dépilement / Pop | du Sommet | (de la valeur 2) ", color=none, width=0.5]
  headerIndices [label="", color=none]
  headerValues -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=12, width=0.5, height = 1.5, fixedsize=true];
  values [label="<f0> 4 | <f2> 8 | <f3> 5 ", color=blue, fillcolor=lightblue, style=filled, width=0.5];
  indices [label="<f0> Sommet |  |  |  |  | ", color=white, fontcolor=red, fontsize=12, width=0.7];

  node [shape=plaintext, fontcolor=red, fontsize=12, height = 0.3];
  pop [label="2", fontcolor=blue]
  " . " [label="  ", shape=Mrecord, height=0, fontcolor=none, shape=Mrecord, color=none]
  " " [label=" ", shape=Mrecord, height=0.5, fontcolor=none, shape=Mrecord, color=grey]
  " " -> pop [color=blue, tailclip=false,]

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
}
```

</div>

<clear></clear>
