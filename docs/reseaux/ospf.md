# TNSI : Le Protocole OSPF

Le <bred>protocole OSPF - Open Shortest Path First</bred>[^1] :gb: rentre dans la catégorie des protocoles à état de lien.

Dans le protocole à vecteur de distance que nous venons de voir, on cherche à minimiser le nombre de sauts, mais sans aucune garantie que le chemin emprunté soit en réalité le plus performant (en termes de débit par exemple). De plus avec RIP, chaque routeur ne connaît que ses voisins immédiats, il n'a donc pas connaissance de l'ensemble de la topologie du réseau. Enfin, le protocole RIP est limité aux petits réseaux (15 routeurs maximum) et est assez gourmand en termes de bande passante puisqu'il nécessite l'échange d'un volume de données assez important.
Principe général de l'algorithme

Le protocole OSPF propose une approche tout à fait différente : au lieu de s'intéresser au nombre de sauts, on va chercher à optimiser le débit des liaisons empruntées. Pour cela, chaque routeur va devoir connaître l'intégralité du réseau avec le débit associé à chaque lien afin d'appliquer un algorithme de recherche de chemin optimal.

On peut faire un parallèle entre le fonctionnement d'OSPF et celui de nos logiciels de guidage par GPS. En effet, dans ce type de logiciels :

* l'ensemble de la carte de France et de ses routes est connue du logiciel
* le type de chaque route est renseigné ainsi que la vitesse autorisée sur la route
* le calcul d'itinéraire va permettre le calcul d'un chemin permettant par exemple d'emprunter les routes sur lesquelles la vitesse est la plus importante (temps le plus court).

Cette vidéo de Claude Chaudet (Institut Mines-Télécom) expose le principe du routage à état de lien.

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/-utHPKREZV8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>
Claude Chaudet, Maître de Conférence
IMT - Institut Mines-Télécom
</figcaption>

<iframe width="560" height="315" src="https://www.youtube.com/embed/FeZI3Xl7j84" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

<figcaption>
Claude Chaudet, Maître de Conférence
IMT - Institut Mines-Télécom
</figcaption>

</center>

## Découverte de la Topologie du Réseau

OSPF a besoin de connaître la topologie du réseau ainsi que la qualité de chaque lien en terme de bande passante. Pour cela, chaque routeur va fabriquer une table de voisinage : il s'agit d'un tableau permettant d'identifier tous les routeurs qui lui sont connectés ainsi que le débit associé à chaque lien. Pour obtenir ces information, le routeur échange périodiquement des messages (appelés messages hello) avec ses voisins.

![Hello](../img/hello.png)

| Voisin | Qualité du Lien |
| :-: | :-: |
| $B$ | $1$ Gb/s |
| $C$ | $10$ Gb/s |


Une fois tous ses voisins directs identifiés, le routeur va envoyer sa table de voisinage à tous les autres routeurs du réseau. Il va recevoir des autres routeurs leurs tables de voisinages et ainsi pouvoir constituer une cartographie complète du réseau.

## L'Algorithme de Djikstra

L'algorithme de Djikstra datant de 1959 permet de trouver le chemin le plus court sur un graphe.

### Exemple

Considérons le réseau suivant. Après échanges de messages hello, la cartographie suivante du réseau a été constituée :

![Réseau 3](../img/reseau3.png)

Nous cherchons à déterminer le chemin le plus rapide entre R1 et R7. L'outil [graphonline](https://graphonline.ru/fr?graph=BPTnrZPMWqlGXaGe) vous permet de le faire visuellement via le menu Algorithmes / plus court chemin avec l'algorithme de Djisktra.

![Réseau 4](../img/reseau4.png)

Contrairement à RIP, le chemin qu'OSPF nous indiquera sera R1 => R2 => R3 => R5 => R4 => R6 => R7. Ce chemin n'est clairement pas le plus efficace en termes de sauts mais est le plus rapide en termes de débit car il n'exploite pratiquement que des liaisons à 10 Gb/s.

### Principe de l'Algorithme

<iframe width="560" height="315" src="https://www.youtube.com/embed/MybdP4kice4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Application à notre exemple

Djisktra permet de minimiser la longueur d'un chemin, or nous souhaitons maximiser le débit sur nos liaisons. Nous allons donc considérer l'inverse de la bande passante de nos liens pour appliquer Djisktra : maximiser les débit revient à minimiser l'inverse des débits :

* $1$ Gb/s sera affecté du poids $1$
* $10$ Gb/s sera affecté du poids $0.1$
* $100$ Mb/s sera affecté du poids $10$

Nous allons ensuite constituer notre tableau. A chaque nouvelle ligne, on calcule les distances totales vers les destinations possibles et on ne retient que la plus petite (en gras) que l'on marque sur une nouvelle ligne.

Pour empêcher les retours, une fois une destination choisie (en gras), on désactive tout le reste de la colonne (avec des x)

<center>

| R1 | R2 | R3 | R4 | R5 | R6 | R7 |
| :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| 0 - R1 ||||||
| 0 - R1            | 0.1 - R1 | 1 - R1    |  |  |  |
| x | 0.1 - R1      | 0.2 - R2 | 1.1 R2    |  |  |  |
| x |       x       | 0.2 - R2 | 0.3 - R3  |  |  |  |
| x |       x       |     x    | 0.4 - R5  | 0.3 - R3 | 1.3 - R5 | 10.3 - R5 |
| x |       x       |     x    | 0.4 - R5  |     x    | 0.5 - R4 | |
| x |       x       |     x    | x         |     x    | 0.5 - R4 | 1.5 - R6 |
| x |       x       |     x    | x         |     x    |     x    | 1.5 - R6 |

</center>

Dans le tableau, on indique des couples distance - origine : ainsi 0.5 - R4 dans la colonne R6 signifie que R6 est à une distance minimum de 0,5 du départ en provenance de R4. On peut ainsi reconstituer l'itinéraire optimal en partant de R7 et en remontant à l'envers en utilisant le champ origine :

R1 => R2 => R3 => R5 => R4 => R6 => R7 avec un poids total minimum de 1,5.

## A vous de jouer

On supprime la liaison R4-R5. Réappliquez l'algorithme de Djikstra pour déterminer un chemin optimal entre R1 et R7.

## Conclusion

OSPF peut s'adapter à la qualité des liens mais dans une certaine mesure uniquement : Si un lien à n10 Gb/s est saturé, il vaut mieux emprunter un lien à 1 Gb/s sous utilisé, mais OSPF n'en a pas connaissance.

Il n'y a pas dans l'absolut de meilleur algorithme de routage, tout dépend du réseau auquel on a affaire. Un protocole sera plus réactif face aux changements de topologie mais au prix d'un plus gros volume échangé. Un autre sera plus efficace si les liaisons au sein du réseau sont très hétérogènes.

## Notes et Références

[^1]: Ce cours est directement inspiré du site [lecluse.fr](Olivier Lecluse) d'[Olivier Lecluse](Olivier Lecluse)