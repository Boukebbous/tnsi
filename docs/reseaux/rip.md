# TNSI : Le Protocole RIP 

Le <bred>protocole RIP - Routing Information Protocol</bred> :gb:, mis au point dans les années $1950$, rentre dans la catégorie des protocoles <bred>à vecteur de distance</bred>. Un vecteur de distance est **un couple (adresse, distance)**.

Le principe simplifié de ce protocole est de chercher à **minimiser le nombre de routeurs** à traverser pour atteindre la destination : on minimise le nombre de **sauts** ou **hops**.

## Principe général de l'algorithme

Chaque routeur reçoit en permanence (toutes les $30$ secondes environ) de ses voisins les informations de routage qu'ils possèdent. Il va alors exploiter ces informations pour se construire lui-même sa table de routage en ne retenant que les informations les plus pertinentes : une simple comparaison permet de ne garder que le chemin le plus avantageux. Il transmettra a son tour ces informations à ses voisins et ainsi de suite. C'est l'<bred>algorithme de Belman-Ford</bred> : **un des algorithmes de recherche de plus court chemin dans un graphe**.

A l'issue de quelques étapes, les tables se stabilisent et le routage est pleinement opérationnel. Le temps nécessaire à la stabilisation des tables est proportionnel au diamètre du graphe modélisant le réseau (càd au nombre maximal d'étapes nécessaires pour relier deux points quelconques du réseau).

Regardez cette vidéo de *Claude Chaudet (Institut Mines-Télécom)* qui expose le principe du routage à vecteur de distance.

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/kzablGaqUXM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>
Claude Chaudet, IMT - Institut Mines-Télécom
</figcaption>

</center>

## Un Exemple

Considérons le réseau suivant qui relie deux réseaux d'une entreprise :

* le réseau $1$ contient des postes de travail dans un bureau.
* le réseau $2$ contient un serveur dans un centre de données.

Les routeurs *R1* et *R6* permettent d'accéder au réseau de l'entreprise, *R2*, *R3*, *R4* et *R5*, des routeurs internes au réseau. 

![Réseau 2](../img/reseau2.svg){.center}

Nous allons nous intéresser à l'évolution des tables de routage des routeurs *R1* et *R3* sur lesquels on a activé le protocole RIP.

### Étape 0

Au démarrage, les routeurs *R1* et *R3* ne connaissent que leurs voisins proches. Leurs tables peuvent donc ressembler à ceci :

<center>

| Destination |	Passerelle | Interface | Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| <pre>192.168.1.0</pre> | | wifi0    |     $1$    | ==> vers les postes de travail |
| <pre>172.16.0.0</pre>  | | eth0     |     $1$    | ==> vers *R3* |

</center>

Au départ, *R1* ne peut atteindre que ses voisins immédiats (nb Sauts vaut $1$). Aucune passerelle n'est nécessaire puisque la communication est directe. Chaque sous réseau utilise une interface spécifique. Le réseau local 1 contenant les postes de travail est accessible en wifi.

En ce qui concerne le routeur $3$, celui-ci possède $4$ interfaces réseau filaires, que nous nommerons `eth0-3` qui permettent d'atteindre les routeurs immédiats (*R1*, *R2*, *R4* et *R5*). Voici à quoi peut ressembler sa table de routage au démarrage :

<center>

| Destination |	Passerelle | Interface | Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| <pre>172.16.0.0</pre> | |	eth$0$ | $1$ | ==> vers *R1* |
| <pre>172.16.1.0</pre> | |	eth$1$ | $1$ | ==> vers *R2* |
| <pre>172.16.6.0</pre> | |	eth$2$ | $1$ | ==> vers *R5* |
| <pre>172.16.3.0</pre> | |	eth$3$ | $1$ | ==> vers *R4* |

</center>

### Étape 1

Au bout de $30$ secondes, un premier échange intervient avec les voisins immédiats de chacun des routeurs.

!!! mth "Le principe de l'algorithme"
    Lorsqu'un routeur reçoit une nouvelle route de la part d'un voisin, $4$ cas sont envisageables :

    * Il découvre une route vers un nouveau **réseau inconnu**  
      ==> Il l'ajoute à sa table.
    * Il découvre une route vers un réseau **connu**, **plus courte** que celle qu'il possède dans sa table  
      ==> Il actualise sa table
    * Il découvre une route vers un réseau **connu**, **plus longue** que celle qu'il possède dans sa table  
      ==> Il ignore cette route.
    * Il reçoit une route vers un réseau **connu** en provenance d'un routeur **déjà existant dans sa table**  
      ==> Il met à jour sa table car la topologie du réseau a été modifiée.

En appliquant ces règles, voici la table de routage de *R1* après une étape :

<center>

| Destination | Passerelle | Interface |	Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| 192.168.1.0 |            |  wifi0     |     $1$    | ==> vers les postes de travail |
| 172.16.0.0  |            |	eth0      |     $1$    | ==> vers *R3* |
| 172.16.1.0  | 172.16.0.3 | 	eth0      |     $2$    | Ces 3 routes |
| 172.16.6.0  | 172.16.0.3 | 	eth0      |     $2$    | proviennent |
| 172.16.3.0  | 172.16.0.3 | 	eth0      |     $2$    | de *R3* |

</center>

`172.16.0.3` est l'adresse IP du routeur *R3*. On ajoute à la table précédente les réseaux atteignables par *R3*. On pense cependant à ajouter $1$ au nombre de sauts ! Si *R1* veut atteindre le réseau `172.16.3.0`, il s'adressera à *R3* et atteindra le réseau cible en $2$ sauts.

Voici la table de *R3* qui s'enrichit des informations envoyées par *R1* afin d'atteindre le réseau local, mais aussi des informations en provenance de *R2*, *R4* et *R5*. Il découvre ainsi $4$ nouveaux réseaux.

<center>

| destination |	passerelle | interface | Nb sauts | remarques |
| :-: |	:-: | :-: | :-: | :-: |
| 172.16.0.0 	|            | eth$0$      | $1$ | |
| 172.16.1.0 	|            | eth$1$      | $1$ | |
| 172.16.6.0 	|            | eth$2$      | $1$ | |
| 172.16.3.0 	|            | eth$3$      | $1$ | |
| 192.168.1.0 | 172.16.0.1 | eth$0$      | $2$ | reçu de *R1* |
| 172.16.2.0  | 172.16.1.2 | eth$1$      | $2$ | reçu de *R2* |
| 172.16.5.0  | 172.16.6.5 | eth$2$      | $2$ | reçu de *R5* |
| 172.16.4.0  | 172.16.3.4 | eth$3$      | $2$ | reçu de *R4* |

</center>

### Étape 3

Comme vous le voyez, les tables deviennent vite longues et énumérer dans le détail chacune d'elle est trop long. On va donc passer directement à l'étape finale : l'étape $3$. Voici ce que contient la table de routage de *R1* :

<center>

| Destination | Passerelle | Interface | Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| 192.168.1.0 |            | wifi0 | 1 | ==> vers les postes de travail |
| 172.16.0.0  |            | eth0  | 1 | ==> vers R3 |
| 172.16.1.0  | 172.16.0.3 | eth0  | 2 | |
| 172.16.6.0  | 172.16.0.3 | eth0  | 2 | |
| 172.16.3.0  | 172.16.0.3 | eth0  | 2 | |
| 172.16.5.0  | 172.16.0.3 | eth0  | 3 | obtenu à l'étape 2 |
| 192.168.2.0 | 172.16.0.3 | eth0  | 4 | obtenu à l'étape 3 |

</center>

Comme vous le voyez, le routeur *R1* est à présent en capacité d'acheminer un paquet du poste de travail du réseau 1 vers le serveur se trouvant dans le réseau 2.

## Détection des Pannes

Le protocole RIP est en mesure de détecter des pannes : Si un routeur ne reçoit pas d'information de la part d'un de ses voisins au bout d'un temps de l'ordre de 3 minutes (configurable) il va considérer que ce lien est mort et en informer ses voisins en indiquant un nombre de sauts égal à $16$. Puisque RIP ne gère que $15$ sauts au maximum, $16$ peut être considéré comme une distance infinie.

De cette manière, les voisins vont pouvoir recalculer leurs routes en conséquence en évitant le lien qui est tombé.

## Détection des Boucles

RIP implémente d'autres mécanismes pour empêcher que se forment des boucles de routage. Une boucle est par exemple une route du type `R2 -> R3 -> R5 -> R2`. Des exemples de tels mécanismes sont :

* une **durée de vie** limitée sur les paquets (TTL - Time To Live) afin qu'un paquet qui tourne en rond soit détruit
* ne pas renvoyer une information vers un routeur si celle-ci est déjà passée par ce routeur

## A vous de jouer

### Scénario 1

Élaborez au fil du temps la table de routage du routeur R4 de manière similaire à ce que l'on vient de faire.

### Scénario 2 : Travail en groupe

* En constituant $6$ groupes dans la classe, vous élaborerez en suivant le protocole RIP votre table de routeur et l'échangerez au fur à mesure avec vos voisins afin de finaliser la table de routage du réseau.

* Vous simulerez la suppression du lien entre R3 et R5 et continuerez l’algorithme afin d'observer la mise à jour des tables de routage.

## Conclusion

Le protocole RIP est en général utilisé sur de petits réseaux : il est en effet limité à $15$ sauts et il génère de plus un trafic important.

Pour des structures plus importantes, on va lui préférer le protocole OSPF.