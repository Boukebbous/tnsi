# Principe du Routage

Le travail d'un réseau consiste essentiellement à trouver les bons chemins pour amener chaque paquet à sa destination.

Un paquet de données qui doit traverser un réseau n'a a priori aucune idée du chemin qu'il va devoir suivre. Il fait entièrement confiance aux indications fournies par les routeurs, espérant que celles-ci soient les plus fiables possibles.

## Prérequis

<iframe width="100%" height="600" src="https://www.youtube.com/embed/s18KtOLpCg4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dans la suite, nous allons étudier en détails deux exemples de protocoles de routage (RIP et OSPF). Afin de comprendre les enjeux de ces protocoles de routage, voici une petite vidéo introductive (6'30) expliquant pourquoi et comment les routeurs communiquent entre eux afin de déterminer le meilleur itinéraire à faire emprunter aux paquets dont ils ont la charge.

<iframe width="100%" height="600" src="https://www.youtube.com/embed/sT9-IcbjqzI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>
Claude Chaudet, Maître de Conférence IMT (Institut Mines-Télécom)
</figcaption>

## Le Routeur

La box que vous avez chez vous est un routeur également qui possède plusieurs interfaces réseau :

* une interface est connectée au réseau de votre opérateur (FAI).
* une interface filaire (ethernet) connectée à votre réseau local
* une interface Wifi

Un routeur sur internet est un peu plus sophistiqué, possède souvent plus de ports et ressemble d'extérieur à un switch. Il dispose d'un logiciel interne bien plus sophistiqué afin de lui permettre de communiquer avec ses routeurs voisins pour l'aider à déterminer les meilleures routes à emprunter pour acheminer ses paquets.

<figure>

![Routeur Cisco](../img/routeur-cisco.png){.center}
<figcaption>
Routeur Cisco - WS-C2960X-48FPD-L - Catalyst 2960-X
<figcaption>

<figure>

Le routeur reçoit un paquet sur l'un de ses ports et sa mission consiste essentiellement à déterminer sur lequel de ses autres ports il doit réacheminer le paquet. Il doit le faire très vite et s'adapter à un environnement qui change (routes qui apparaissent et disparaissent).
Table de routage

Pour choisir le bon chemin, le routeur s'appuie sur une <bred>table de routage</bred> : c'est une table donnant pour chaque <bred>destination</bred> connue par le routeur la porte de sortie à emprunter ainsi que l'efficacité de cette route. Cette efficacité sera exploitée dans les algorithmes que nous détaillerons par la suite.

En voici un exemple :

<pre>
<code>
Total number of IP routes: 687
Destination     NetMask             Gateway         Port    Cost
137.194.2.0     255.255.254.0       137.194.4.254       v10     2
137.194.4.0     255.255.255.248     137.194.4.253       v10     2
137.194.4.8     255.255.255.248     137.194.4.251       v10     11
137.194.4.192   255.255.255.192     0.0.0.0             v10     1
137.194.6.0     255.255.254.0       137.194.4.254       v10     2
137.194.8.0     255.255.248.0       137.194.4.251       v10     20
137.194.16.0    255.255.255.128     137.194.160.230     v160    11
137.194.16.128  255.255.255.128     137.194.192.102     v192    11
137.194.16.144  255.255.255.240     137.194.192.102     v192    11
137.194.16.176  255.255.255.240     137.194.192.103     v192    20
137.194.17.0    255.255.255.0       137.194.192.103     v192    2
</code>
</pre>

Le routeur sait ainsi que pour atteindre la machine `137.194.2.21`, il doit s'adresser au réseau `137.194.2.0/23` en redirigeant le paquet au routeur `137.194.4.254` qui est joignable sur l'interface `v10`.

## Protocoles de Routage

Imaginons le petit réseau suivant : ![Exemple de réseau](../img/reseau1.png)

Aller du réseau 5 (*R5*) au réseau 2 (*R2*) revient à trouver son chemin dans un graphe :

* les routeurs constituent les noeuds de ce graphe
* les liaisons (cuivre ,fibre, wifi, 3/4/5G, satellite ...) constituent les arêtes

Plusieurs algorithmes sont possibles. Néanmoins pour le routage réseau, des contraintes particulières liées au fonctionnement des réseaux sont à prendre en compte :

* les routeurs n'ont pas connaissance de la topologie globale du réseau, ils ne communiquent qu'avec leur voisin immédiats.
* l'algorithme est <bred>distribué</bred> : il n'y a pas en général de centre de gestion central. Chaque routeur se constitue sa propre table lui même à partir des informations communiquées par ses voisins. Lui même transmet à ses voisins les informations en sa possession.
* l'algorithme est <bred>itératif</bred> : il est exécuté en permanence et ne s'arrête jamais. Lorsqu'une modification est faite sur le réseau, celle-ci se propage de proche en proche à chaque routeur qui adapte sa table en conséquence.

Sur notre réseau d'exemple, *R2* signale sa présence à *R1* et *R4*. *R4* se signale à *R5* et indique aussi qu'il peut joindre *R2*. De proche en proche, les tables de routage de chacun se propagent. Pour aller de *R2* à *R5*, *R2* a le choix entre plusieurs chemins :

* `R2 -> R4 -> R5`
* `R2 -> R1 -> R3 -> R5`
* `R2 -> R1 -> R3 -> R6 -> R5` etc...

À partir de la, deux grandes familles d'algorithmes se dégagent :

* le rougage <bred>à vecteur de distance</bred>. Par exemple: RIP
* le routage <bred>à état de lien</bred>. Par exemple: OSPF

La première famille consiste à compter le nombre d'étapes nécessaires pour atteindre l'objectif. Dans notre exemple, `R2 -> R4 -> R5` est clairement avantageux. Mais il se peut que la liaison `R4 -> R5` utilise une liaison satellite à bas débit et grande latence. On a alors une seconde famille d’algorithmes a état de liens qui va prendre en compte la qualité des liaisons pour optimiser le débit ou la latence de la liaison.

Nous allons maintenant étudier le <bred>protocole RIP - Routing Information Protocol</bred> :gb: qui rentre dans la première famille et le <bred>protocole OSPF - Open Shortest Path First</bred> :gb: qui est de la seconde famille de protocoles.
