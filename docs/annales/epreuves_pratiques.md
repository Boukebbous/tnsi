---
toc:
  toc_depth: 2
---

# TNSI : Épreuves Pratiques

Page de Référence: [éduscol](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi)

{{ epreuves_pratiques() }}

