# Cours SGBD

## Introduction

### SGBD

<def>

Un <bred>Système de Gestion de Bases de Données (SGBD)</bred> est un ensemble d'outils informatiques permettant de réaliser des opérations de bases (CRUD), ou plus évoluées, sur les données :

* <bred>C</bred>**reate** ou **Création/Sauvegarde**
* <bred>R</bred>**ead** ou **Lecture/Interrogation**
* <bred>U</bred>**pdate** ou **Mise à Jour**
* <bred>D</bred>**elete** ou **Suppression**

Selon le SGBD, d'autres opérations plus évoluées sont permises.

</def>

Un SGBD est donc un ensemble logiciel permettant aux programmeurs de réaliser des opérations sur des données spécifiques, contenues dans une grande masse d'information partagées par plusieurs utilisateurs.

### Base de Données

<def>

Une <bred>Base De Données (BDD)</bred> :fr: ou <bred>DataBase (DB)</bred> :gb: est la structure où sont stockées les données

</def>

### SGBD vs BDD

* Le SGBD est en charge de toute la gestion, la configuration, l'administration et la sécurité de la structure de la BDD.
* La BDD ne s'occupe que du stockage des données en elles-mêmes, et des relations entre ces données

## Fonctions d'un SGBD

Selon le SGDB considéré, il **peut** également permettre d'autres opérations plus évoluées, notamment :

* la **mise en forme** des données: description des types de données par des noms, des formats, etc...
* l'**authentification** des utilisateurs. Ce
* **sécurisation/protection** des données :
  Pourquoi? pour réduire le risque de vol de :
  * propriété intelectuelle
  * documents professionnels
  * de données médicales,
  * d'emails
  * de secrets commerciaux, etc..

  Quelques Techniques Utilisées: 
  * gestion de Droits
  * gestion de l'identité
  * gestion de l'accès
  * détection des menaces
  * analyse de la sécurité

* **intégrité** des données : regroupe un ensemble de critères s'intéressant (intuitivement) à la **qualité de l'état actuel des données, et leur préservation dans ce bon état**, notamment:
  * la **fiabilité/exactitude** des données.
  * la **cohérence** des données
  
  Quelques règles que doivent respecter les données, pour garantir l'intégrité initiale des données, et pour la préserver:
  * validation des entrées
  * validation des données
  * suppression des doublons
  * sauvegarde des données
  * contrôles d'accès: gestion des droits utilisateurs et protection des accès physiques aux serveurs, etc..
  * audits automatiques permettant de remonter à la source en cas de vol de données

* **optimisation des performances** (minimisation des temps de recherche notamment)
* **outils de gestion**:
  * métadonnées : 
* **outils d'administration**
* **cloud** : Le SGBD peut offrir une indépendance logique et physique des données. Cela signifie qu'il peut protéger les utilisateurs et les applications du besoin de savoir où les données sont stockées où d'avoir à s'inquiéter des changements dans la structure physique des données (stockage et matériel).

<exp data="de SGBD classiques">

Quelques exemples nominatifs de SGBD classiques et usuels en 2021 :

* ***[Oracle Database](https://www.oracle.com/fr/database/)*** appelé **Oracle DB**. Principaux Clients : Grandes Entreprises: Banques, Défense, Sécurité, $\approx $48% du parc mondial. **Oracle Corporation** est une entreprise américaine fondée en $1977$ par **Larry Ellison**, leader mondial dans le domaine des Bases de Données (BDD). 
Payant (cher : $10 To$ $\approx 1800$ €/mois en $2018$). Quelques Exemples de Points Forts:
  * Gestion de données:
    * pools de session
    * connexions
    * gestion de larges objets
  * Sécurisation des Données : Évaluation, détection et prévention des menaces de sécurité, grâce au cryptage, gestion des clés, masquage des données, contrôles d'accès privilégiés, surveillance des activités et audit.
  * Administration : Gestion, Sécurisation, et Maintenance automatisées des BDD, par Machine Learning
  * Cloud
* ***[4D](https://fr.4d.com/)*** (4ième Dimension)
* ***[Microsoft SQL Server](https://www.microsoft.com/fr-fr/sql-server/sql-server-2019)***
* ***[SQLite](https://www.sqlite.org/index.html)*** : Open Source et Gratuit, pour les BDD *légères* (petit site, applications sur tél portables)
* ***[MySQL](https://www.mysql.com/fr/)*** : initialement Open Source et Gratuit, racheté depuis par ***Sun Microsystems*** en $2008$ pour 1 milliard de dollars..., puis ***Oracle*** a racheté **Sun** en $2009$). Très Utilisé pour les sites Web et intégration PHP
* ***[MariaDB](https://mariadb.org/)*** est le remplaçant de *MySQL* dans le monde de l'Open Source Gratuit, depuis le rachat de *MySQL* par *Oracle* en $2009$.
* ***[PostgreSQL](https://www.postgresql.org/)*** Open Source et Gratuit. Un peu plus complet que MySQL.
* ***[NoSQL]()*** : leader pour les **Centres de Données** :fr: ou les **Data Center** :gb:, et donc le **Big Data**

</exp>

## Les Propriétés *ACID*

En plus des fonctions précédentes, citées dans la définition, un SGBD :

* assure le partage des données
* garantit qu'une opération est réalisée de façon **fiable**, notamment grâce à des propriétés classiques, appelées les <bred>*Propriétés ACID*</bred> :
  * L'<env><bred>A</bred>***tomicité***</env> vérifie qu'une opération s'effectue entièrement, ou pas du tout
  * La <env><bred>C</bred>***ohérence***</env> assure que chaque transaction amèrera le système d'un état ***valide*** à un autre état ***valide***, en tenant compte notamment, mais pas exclusivement, des **contraintes d'intégrité**, des **rollbacks** en cascade, des **déclencheurs** et combinaisons d'**événements**.
  * <env><bred>I</bred>***solation***</env> : toute transaction doit s'exécuter comme si elle était la seule sur le système. Il ne doit y avoir aucune dépendance entre transactions, de sorte qu'une exécution simultanée de plusieurs transactions produise le même état que celui qui serait obtenu par l'exécution en série des transactions.
  * La <env><bred>D</bred>***urabilité***</env> assure que lorsqu'une transaction a été enregistrée, elle demeure enregistrée même à la suite d'une panne d'électricité, d'ordinateur ou d'un autre problème.

## Les Différents Modèles de SGBD

Un **modèle de SGBD** illustre la **structure logique** du SGBD.
Il existe de nombreux modèles de SGBD, parmi les plus courants :

### Le modèle Hiérarchique

Les données sont stockées hiérarchiquement, selon une arborescence descendante (comme dans un arbre). 

* Ce modèle utilise des **pointeurs unidirectionnels** (chaque **enregistrement** mène vers uniquement vers un autre) entre les différents **enregistrements**. 
* Requêtes procédurales (comment aller chercher les données, plutôt que quoi aller chercher)
* modèle **défini sur des produits technologiques** et non sur un modèle abstrait (d'où une grande dépendance aux technologies)
* produit le plus connu: Information Management System (IMS) par IBM ($1968$)
Historiquement, il s'agit du premier modèle de SGBD.

<center>

```dot
digraph G {
  node [shape=rectangle, label=""]
  //node [shape=rectangle]
  a -> b, c
  b -> d, e
  c -> f, g, h
  a [label="Produit"]
  b [label="Commerciaux"]
  c [label="Techniciens"]
  d [label="Ambulants"]
  e [label="Sédentaires"]
  f [label="Web"]
  g [label="Graphistes"]
  h [label="Sécurité"]
}
```

<figcaption>Le modèle Hiérarchique</figcaption>

</center>

Ce modèle Hiérarchique est aujourd'hui **obsolète**.

### Le modèle Réseau

Le **modèle réseau** est une extension du modèle hiérarchique, et lui succède historiquement : celui-ci utilise donc encore des pointeurs vers des enregistrements. Il autorise même des relations **plusieurs à plusieurs** entre des enregistrements liés, ce qui autorise plusieurs (ou pas) enregistrements parents.
La structure n'est donc **plus obligatoirement descendante**. 
Son pic de popularité remonte aux années $1970$, après qu'il ait été officiellement défini par la conférence sur les langages de système de traitement des données (Conference on Data Systems Languages, CODASYL)

<center>

```dot
digraph G {
  node [shape=rectangle, label=""]
  //node [shape=rectangle]
  a -> b, c
  b -> c, d, e
  c -> f
  c -> g
  c -> h [weight=2]
  d -> e
  f -> g
  d -> i
  {rank=same; b,c}
  {rank=same; d,e}
  {rank=same; f,g}
}
```

<figcaption>Le modèle Réseau</figcaption>

</center>

Ce modèle Réseau est aujourd'hui **obsolète**.

### Le modèle Relationnel (SGBDR)

Les données sont enregistrées dans des **Tableaux appelés** <bred>Tables</bred> **, à deux dimensions :** <bred>lignes</bred> **et** <bred>colonnes</bred>

<center>

<div style="float: left; margin-left:40%; margin-right:2em;">

| $\,$ | $\,$ | $\,$ |
|:-:|:-:|:-:|
| $\,$ | $\,$ | $\,$ |
| $\,$ | $\,$ | $\,$ |
| $\,$ | $\,$ | $\,$ |
| $\,$ | $\,$ | $\,$ |

</div>

<div style="float: left;">

| $\,$ | $\,$ | 
|:-:|:-:|
| $\,$ | $\,$ |
| $\,$ | $\,$ |
| $\,$ | $\,$ |

</div>

</center>

<clear></clear>

* Le modèle relationnel est **le plus utilisé actuellement**, et ce depuis la fin des années $1990$ : environ les $\displaystyle \frac 34$ des SGBD/bases de données utilisent ce modèle. 

* <env>**Limites**</env> L'évolution entre autres du **matériel informatique**, des **langages de programmation**, des exigences des **interfaces-utilisateurs**, des **applications multimédia** ainsi que de la **réseautique** a mis en lumière des limitations du modèle relationnel, et a ainsi poussé d'autres modèles à émerger. Plus particulièrement, ses limites pour les **systèmes distribués à grande échelle** sur le Web (comprendre les **Centres de Données** :fr:, ou **Data Centers** :gb: utilisés pour le **Big Data**), comme par exemple ***Twitter*** et ***Facebook***, ont conduit à l'apparition des familles de bases de données appelées **familles NoSQL (Not Only SQL)**

Dans toute la suite, nous ne nous intéresserons qu'au ce modèle relationnel


### Le modèle Déductif

Les données sont présentées sous forme de Tables, comme le modèle relationnel, mais la manipulation des tables se fait différemment (par calcul des prédicats : hors-programme)

### Le modèle Objet (SGBDO)

Les données sont stockées sous forme d'Objets, c'est à dire en pratique sous forme de ***Classes***, présentant des données membres.
Les champs sont des instances de la classe.

### Familles NoSQL

* L'appellation <bred>*NoSQL (Not only SQL)*</bred> date de $2009$. **Il ne s'agit pas, à proprement parler, d'un *modèle NoSQL***, mais de plusieurs ***familles NoSQL***, qui répondent à des besoins différents. En effet, le modèle relationnel, se révèle **peu efficace** et rencontre **quelques limites** dans sa manière de représenter et de manipuler les données, particulièrement dans le contexte d'environnements Web distribués à grande échelle, comprendre pour les **Centres de Données** :fr:, ou **Data Centers** :gb:, qui sont amenés à traiter de très grands volumes de données (ce que l'on appelle le **Big Data**, p. ex ***Twitter***, ***Facebook***, ***eBay***).

* Les ***familles NoSQL***, délaissent les propriétés des transactions relationnelles qui ont été pensés pour garantir et de maintenir la cohérence des données (**propriétés ACID**) au profit de contraintes qui priorisent la **disponibilité** des données (**contraintes BASE**).

* <env>**Exemples de Familles NoSQL**</env>
  * **Familles Orientées Graphes**: Encore plus flexible qu'un modèle réseaun car il permet la connexion entre tous les noeuds, quels qu'ils soient. Exemple: ***Twitter***
  * **Orientés Colonnes**
  * **Orientés Clés/Valeurs**. Exemple: Système de Sauvegarde type ***Dropbox***
  * **Orientés "*Document*"** : Gestion des documents et/ou des données semi-structurées (e.g. métadonnées), plutôt que des données atomiques. Exemple : métadonnées des produits sur eBay.
* <env>**Exemples d'Implémentations**</env>
  * MongoDB (OpenSource, orienté document de types JSON)
  * Amazon Dynamo DB
  * Cassandra (Facebook)
  * Apache HBase (pour la base de données Big Table de Google)
  * Oracle NoSQL Database
  * etc..

## Le modèle relationnel

### Histoire

<clear></clear>

<div style="width:40%; float:right;margin:0; padding:0;">

<center>

<figure style="width:48%; float:left;">
<img src="img/codd.jpg" alt="Frank Edgar 'Ted' Codd, 1970">
<figcaption>Frank Edgar 'Ted' Codd, 1970</figcaption>
</figure>

<figure style="width:48%; float:right;">
<img src="img/ellison.png" alt="Larry Ellison">
<figcaption>Larry Ellison</figcaption>
</figure>

</center>

</div>

C'est l'informaticien britannique **[Edgar Frank "Ted" Codd](https://fr.wikipedia.org/wiki/Edgar_Frank_Codd)** qui  définit formellement (comprendre mathématiquement) le modèle relationnel en $1970$, avant toute implantation. 
Dix années de recherche auront été nécessaires avant de publier son article fondateur "**A Relationnal Model of Data for Large Shared Data Banks (le $6$ Juin $1970$)**" (*un modèle de données relationnel pour de grandes banques de données*).
Il est donc assez indépendant des technologies de constructeurs/développeurs, et a bien vieilli. C'est donc un "vrai modèle". Bien que ***Codd*** travaillât pour **IBM (International Business Machines)** à cette époque, cette société n'a pas souhaité immédiatement concrétiser ses recherches dans une application concrète, venant de mettre sur le marché **IBM Information Management System**, conçu dans le cadre du **Programme Apollo**. En $1977$, **Larry Ellison** fonde la société **Oracle**, qui devient l'éditeur de la base de données **Oracle DB**, premier produit inspiré par le modèle relationnel, lui-même inspiré par les travaux de **Codd**.

### Représentation des Données en Tables

Le modèle relationnel consiste à représenter les données dans des tableaux, appelés <bred>Tables</bred> ou <bred>Entités</bred>.
Chaque table est composée de <bred>champs</bred>/colonnes, encore appelés <bred>attributs</bred> (comme en POO).

<exp data="Site Internet de VOD - Vidéo à la Demande">

On prend l'exemple d'un **site internet** de VOD dans laquelle:

* un utilisateur dispose d'une carte client (éventuellement numérique)
* un utilisateur peut louer des vidéos à la demande
* trois types d'abonnements sont disponibles : *Free*, *Silver*, *Premium*

Pour se créer un compte le site demande les informations suivantes, stockées dans une table:

<center>

|utilisateur |||
|:-:|:-:|:-:|
| prenom | nom | email |

<figcaption>Table <b>utilisateur</b></figcaption>

</center>

Une fois la structure de Table connue, on peut y insérer des données dans des **enregistrements** (**lignes**).

<center>

|utilisateur |||
|:-:|:-:|:-:|
| prenom | nom | email |
| Daenerys | TARGARYEN | daenerys.targaryen@gmail.com |
| Jon | SNOW | jon.snow@gmail.com |
| Cersei | LANNISTER | cersei.lannister@gmail.com |
| ... | ... | ... |

<figcaption>Insertion de données dans la Table Utilisateur</figcaption>

</center>

</exp>

### Enregistrements, Champs/Attributs. Domaine. Degré

<def>

Dans une Table :

* Chaque **ligne** d'une table est un <bred>enregistrement</bred> :fr: (ou un <bred>record</bred> :gb:)
* Chaque **colonne** d'une table est appelée un <bred>champ</bred> :fr: (ou <bred>field</bred> :gb:) ou un <bred>attribut</bred> (ou <bred>attribute</bred> :gb:)
* Le <bred>domaine</bred> d'un champ/attribut est le type des données qu'il contient: entier, flottant, chaîne de caractères, booléen, etc...
* Le <bred>degré</bred> d'une table est le nombre de champs/attributs

</def>

<exp>

* un enregistrement de la table utilisateur est (par exemple):
`(Daenerys, TARGARYEN, daenerys.targaryen@gmail.com)`
* la table **utilisateur** dispose de $3$ champs/attributs : `prenom`, `nom` et `email`
* il s'agit donc d'une trable de **degré** $3$
* le domaine du champ `prenom` est `str` (chaine de caractère)

</exp>

### Schéma d'une Table

<def>

Le <bred>schéma</bred> :fr: ou <bred>schema</bred> :gb: d'une table est l'ensemble de :

* ses champs/attributs et 
* des domaines associés (=types de données)

</def>

<exp>

Le **schéma** de la table `utilisateur` est `(prenom:str, nom:str, email:str)`

</exp>

### Clés Primaires et Étrangères. Notion de Relation entre Tables

Il est fréquent de souhaiter disposer dans une même table, d'un **identifiant unique** (**id**) pour chaque enregistrement :

<def>

* une <bred>clé primaire</bred> d'une table est le champ/attribut qui permet d'**identifier de manière unique** (sans aucun risque de doublon)
* une <bred>clé étrangère</bred> est un champ/attribut qui fait référence à la **clé primaire d'une autre table**. Une clé étrangère permet de mettre en relation un enregistement d'une table (appelée **table fille**) qui le contient avec un enregistrement de la table référencée (appelée **table parent**)
* Dans ce dernier cas, on dit qu'<bred>il existe une Relation entre ces deux Tables</bred> (entre la table fille et la table mère)

</def>

<exp data="Ajout d'une clé primaire 'id' dans la Table Utilisateur">

<center>

|utilisateur ||||
|:-:|:-:|:-:|:-:|
| **id** | prenom | nom | email |
| 1 | Daenerys | TARGARYEN | daenerys.targaryen@gmail.com |
| 2 | Jon | SNOW | jon.snow@gmail.com |
| 3 | Cersei | LANNISTER | cersei.lannister@gmail.com |
| ... | ... | ... |

<figcaption>Table <b>utilisateur</b> avec une <bred>clé primaire</bred> <em><b>id</b></em></figcaption>

</center>

* Les **champs/attributs** de la table sont : **id**, **prenom**, **nom**, **email**
* le **degré** vaut donc 4
* le domaine de l'attribut **id** est ***entier***. Le domaine des autres attributs est ***str*** (***chaîne de caractère***)

</exp>

<exp data="Ajout d'une Clé Étrangère dans la Table utilisateur">

Considérons maintenant que le site dispose d'une deuxième table **abonnement** : ***Free***, ***Silver***, ***Premium***. 

<center>

|abonnement ||
|:-:|:-:|
| **id_abonne** | type |
| 1 | Free |
| 2 | Silver |
| 3 | Premium |

<figcaption>Table <b>abonnement</b><em> avec une <bred>clé primaire</bred> <b>id_abonne</b></em></figcaption>

|utilisateur |||||
|:-:|:-:|:-:|:-:|:-:|
| **id** | prenom | nom | email | id_abonne |
| 1 | Daenerys | TARGARYEN | daenerys.targaryen@gmail.com | 3 |
| 2 | Jon | SNOW | jon.snow@gmail.com | 1 |
| 3 | Cersei | LANNISTER | cersei.lannister@gmail.com | 2 |
| ... | ... | ... |

<figcaption>Table <b>utilisateur</b> avec une <bred>clé primaire</bred> <em><b>id</b></em><br/> et une <bred>clé étrangère</bred> <em><b>id_abonne</b></em></figcaption>

</center>

</exp>

On a mis en relation deux tables en considérant que l'attribut ***id_abonne*** de la table ***utilisateur*** correspond à l'attribut ***id_abonne*** de la table ***abonnement***

<env>**Interprétation d'un Enregistrement**</env>

* ***Daenerys TARGARYEN*** dispose d'un abonnement ***Premium***, car son `id_abonne` vaut 3
* ***Jon SNOW*** dispose d'un abonnement ***Basic***, car son `id_abonne` vaut 1
* etc...

### Bases De Données (Relationnelles)

<def>

Une <bred>Base De Donnée (BDD)</bred> :fr: ou <bred>DataBase (DB)</bred> :gb: est un ensemble de tables, dont certaines peuvent avoir une relation entre elles.

</def>

## Différents Types Relations entre Tables

### Exemple de Schématisation de Relation entre tables

On peut schématiser une relation existant entre les deux tables précédentes (utilisateur et abonnement) comme suit:

<center>

<div style="float:left; width:30%;">

```mermaid
classDiagram
    class utilisateur {
      🔑 id
      prenom
      nom
      email
      🔑 id_abonne
    }
    class abonnement {
      🔑 id_abonne
    }
    utilisateur -- abonnement
```

<figcaption>
<b>certains logiciels</b> notent 🔑 pour <br/>
les <b>clés primaires</b> et/ou <b>étrangères</b>
</figcaption>

</div>

<div style="float:left; width:35%;">

```mermaid
classDiagram
    class utilisateur {
      * id
      prenom
      nom
      email
      # id_abonne
    }
    class abonnement {
      * id_abonne
    }
    utilisateur -- abonnement
```

<figcaption>
Autres Notations Possibles : <br/>
<enc><bred>* Clé primaire</bred></enc> ou <enc><bred>PK (Primary Key) Clé primaire</bred></enc> <br/><enc><bblue># Clé étrangère</bblue></enc> ou <enc><bblue>FK (Foreign Key) Clé étrangère</bblue></enc>
</figcaption>

</div>

<div style="float:right; width:30%;">

```mermaid
classDiagram
    class utilisateur {
      * id
      # id_abonne
      -----------------
      prenom
      nom
      email
    }
    class abonnement {
      * id_abonne
    }
    utilisateur -- abonnement
```

<figcaption>
Pour plus de clarté, il <em><b>peut arriver</b></em> <br/> qu'on réunisse préalablement et séparément <br/><b>la clé primaire</b> et <b>la/les clés étrangères</b> :<br/> nous ne le ferons pas.
</figcaption>

</div>

</center>

<clear></clear>

### Différents Types de Relations et leur.s Schématisation.s

Néanmoins, cette simple schématisation - un trait simple -  est imparfaite et ne suffit pas totalement à résumer le lien entre deux tables quelconques. En effet, revenons à notre site internet proposant de louer des vidéos:

* un utilisateur dispose d'un seul abonnement
* mais un même abonnement peut être choisi par plusieurs utilisateurs

La schématisation précédente ne permet en effet pas de rendre compte de ces subtilités.
Pour combler cela on distingue plusieurs types de relations entre tables. Il faut savoir que, pour schématiser la relation entre les tables, il existe (au moins) deux notations concurrentes : 

* la notation <bred>UML : Unified Modeling Language</bred>
* la notation <bred>ERD : Entity Relation Diagram</bred>

<mth data="Schématisation des Relations">

On ajoute trois informations à la relation symbolisée par un trait entre les tables :

* à gauche : un symbole représentant le <bred>min</bred> (encore appelé <bred>ordinalité</bred>) qui modélise le nombre **minimum** de fois qu'une instance dans une table/entité (sortante) peut être associée à une instance de l'autre table/entité (cible)
* à droite : un symbole représentant le <bred>max</bred> (encore appelé <bred>cardinalité</bred>) qui modélise le nombre **maximum** de fois qu'une instance dans une table/entité (sortante) peut être associée à une instance de l'autre table/entité (cible)
* l'<bred>association</bred> (en général un verbe, un extrait de phrase) qui détaille plus précisément la relation entre ces deux tables/entités

</mth>

### Relations `1` à `1`

<center>

<div style="width:45%;float:left;margin-right:1em;">

```mermaid
classDiagram
    class utilisateur {
     * id
     prenom
     nom
     email
     # id_carte
    }
    class carte_client {
      * id_carte
      # id_utilisateur
      validite
    }
    utilisateur "1" -- "1" carte_client:appartenance
```

<env>**Notation UML**</env>

| Symbole <br/> $min..max$ <br/> $min,max$ | Signification |
|:-:|:-:|
| $0$ | Zéro |
| $1..1$ <br/> (quelquefois $1$) | Un |
| $1$ <br/> $0..1$ <br/> $0,1$ | Zéro ou Un |
| $n$ <br/> $0..n$ <br/> $0,n$ | Zéro ou Plusieurs |
| $1..n$ <br/> $1,n$ <br/> (quelquefois $n$) | Un ou Plusieurs |
| $\infty$ | une infinité |

<env>**Signification**</env>

Un utilisateur "est le propriétaire" d'exactement **une** carte client
Une carte client appartient exactement à **un** utilisateur

<env>**Association**</env>

***Appartenance*** : doit quelquefois être pensée à l'envers<br/>
quand on la lit dans l'autre sens...

</div>

<div style="width:45%;float:left;">

```mermaid
erDiagram
    utilisateur ||--|| carte_client : "appartenance"
    utilisateur {
        int id
        str prenom
        str nom
        str email
        int id_carte
    }
    carte_client {
        int id_carte
        int id_utilisateur
        str validite
    }
```

<env>**Notation ERD**</env>

| Symbole | Signification |
|:-:|:-:|
| <img src="img/erdOne.png" style="width:100%; box-shadow:none;"> | Un |
| <img src="img/erdMany.png" style="width:100%; box-shadow:none;"> | Plusieurs <br/> (ou $\infty$) |
| <img src="img/erdOneOnlyOne.png" style="width:100%; box-shadow:none;"> | Un (et seulement Un) |
| <img src="img/erdZeroOne.png" style="width:100%; box-shadow:none;"> | Zéro ou Un |
| <img src="img/erdOneMany.png" style="width:100%; box-shadow:none;"> | Un ou Plusieurs <br/>(ou $\infty$) |
| <img src="img/erdZeroMany.png" style="width:100%; box-shadow:none;"> | Zéro ou Plusieurs <br/>(ou $\infty$) |


<env>**Signification: *Idem***</env>

<env>**Association: *Idem***</env>

</div>

</center>

<clear></clear> 

### Relations `1` à `n` (plusieurs)

<center>

<div style="width:45%;float:left;margin-right:1em;">

```mermaid
classDiagram
    class utilisateur {
     * id
     prenom
     nom
     email
     # id_abonne
    }
    class abonnement {
      * id_abonne
      type
    }
    utilisateur "1" -- "n" abonnement:est associé à
```

<env>**Signification**</env>

Un utilisateur *est associé à* **aucun ou un seul** type d'abonnement
Un type d'abonnement *est associé à* **aucun ou plusieurs** utilisateurs

<env>**Association**</env>

"est associé à"

</div>

<div style="width:45%;float:left;">

```mermaid
erDiagram
    utilisateur o|--o{ abonnement : "est associé à"
    utilisateur {
        int id
        str prenom
        str nom
        str email
        int id_abonne
    }
    abonnement {
        int id_abonne
        str type
    }
```

<env>**Signification : *Idem***</env>

<env>**Association : Idem**</env>

</div>

</center>

<clear></clear>

### Relations `n` (plusieurs) à `n` (plusieurs)

<center>

<div style="width:45%;float:left;margin-right:1em;">

```mermaid
classDiagram
    class utilisateur {
     * id
     prenom
     nom
     email
     # id_video
    }
    class video {
      * id_video
      titre
      producteur
      annee
    }
    utilisateur "n" -- "n" video:louer
```

<env>**Signification**</env>

Un utilisateur peut *louer* **aucune ou plusieurs** vidéos
Une vidéo peut être *louée* (à la demande...) par **aucun ou plusieurs** utilisateurs

<env>**Association**</env>

"louer"

</div>

<div style="width:45%;float:left;">

```mermaid
erDiagram
    utilisateur o{--o{ video : "louer"
    utilisateur {
        int id
        str prenom
        str nom
        str email
        int id_abonne
    }
    video {
        int id_video
        str titre
        str producteur
        int annee
    }
```

<env>**Signification : *Idem***</env>

<env>**Association : Idem**</env>

</div>

</center>

<clear></clear>


## Algèbre Relationnelle

L'idée est de donner un sens à des **opérations** entre les tables, inspiré de ce qui se fait en *mathématiques*, en particulier de la ***théorie des ensembles***.

### Union

<def>

Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
L'<bred>Union</bred> des deux tables $T_1$ et $T_2$, notée $T_1 \cup T_2$, est l'ensemble des éléments qui sont $T_1$ ou bien dans $T_2$ :

$$T_1 \cup T_2 = \{ e\in T_1 \text{ ou } e \in T_2 \}$$

</def>

<exp data="Union de deux Tables">

On dispose des tables $T_1$ et $T_2$ suivantes:

<div style="width:45%; margin:auto;">

<div style="float:left;margin-right:2em;">

| $T_1$ |
| :-: |
| **nom** |
| Dupont |
| Dupond |
| Durand |
| Dufour |
| Dumiel |

</div>

<div style="float:left;margin-right:2em;">

| $T_2$ |
| :-: |
| **nom** |
| Dumont |
| Dufour |
| Durville |
| Durail |
| Dupont |

</div>

<div style="float:left;margin-right:2em;">

| $T_1 \cup T_2$ |
| :-: |
| **nom** |
| Dupont |
| Dupond |
| Durand |
| Dufour |
| Dumiel |
| Dumont |
| Durville |
| Durail |

</div>

</div>

</exp>

<clear></clear>

### Intersection

<def>

Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
L'<bred>Intersection</bred> des deux tables $T_1$ et $T_2$, notée $T_1 \cap T_2$, est l'ensemble des éléments qui sont $T_1$ et aussi dans $T_2$ :

$$T_1 \cap T_2 = \{ e\in T_1 \text{ et } e \in T_2 \}$$

</def>

<exp data="Intersection de deux Tables">

On dispose des tables $T_1$ et $T_2$ suivantes:

<div style="width:45%; margin:auto;">

<div style="float:left;margin-right:2em;">

| $T_1$ |
| :-: |
| **nom** |
| Dupont |
| Dupond |
| Durand |
| Dufour |
| Dumiel |

</div>

<div style="float:left;margin-right:2em;">

| $T_2$ |
| :-: |
| **nom** |
| Dumont |
| Dufour |
| Durville |
| Durail |
| Dupont |

</div>

<div style="float:left;margin-right:2em;">

| $T_1 \cap T_2$ |
| :-: |
| **nom** |
| Dupont |
| Dufour |

</div>

</div>

</exp>

<clear></clear>

### Différence

<def>

Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
La <bred>Différence</bred> des deux tables $T_1$ et $T_2$, notée $T_1 - T_2$, est l'ensemble des éléments qui sont $T_1$ mais pas dans $T_2$ :

$$T_1 - T_2 = \{ e\in T_1 \text{ et } e \not \in T_2 \}$$

</def>

<exp data="Différence de deux Tables">

On dispose des tables $T_1$ et $T_2$ suivantes:

<div style="width:45%; margin:auto;">

<div style="float:left;margin-right:2em;">

| $T_1$ |
| :-: |
| **nom** |
| Dupont |
| Dupond |
| Durand |
| Dufour |
| Dumiel |

</div>

<div style="float:left;margin-right:2em;">

| $T_2$ |
| :-: |
| **nom** |
| Dumont |
| Dufour |
| Durville |
| Durail |
| Dupont |

</div>

<div style="float:left;margin-right:2em;">

| $T_1 - T_2$ |
| :-: |
| **nom** |
| Dupond |
| Durand |
| Dumiel |

</div>

</div>

</exp>

<clear></clear>

### Produit Cartésien

<def>

Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
Le <bred>Produit Cartésien</bred> des deux tables $T_1$ et $T_2$, notée $T_1 \times T_2$, est l'ensemble des couples $(e_1,e_2)$ lorsque $e_1$ parcourt $T_1$, et $e_2$ parcourt $T_2$

$$T_1 \times T_2 = \{ (e_1,e_2) \,\, | \,\, e_1 \in T_1 \text{ et } e_2 \in T_2 \}$$

</def>

<exp data="Produit Cartésien de deux Tables">

On dispose des tables $T_1$ et $T_2$ suivantes:

<div style="width:80%; margin:auto;">

<div style="float:left;margin-right:2em;">

| $T_1$ | |
| :-: | :-:
| **id** | **nom** |
| 1 | Dupont |
| 2 | Dufour |
| 3 | Dumiel |

</div>

<div style="float:left;margin-right:2em;">

| $T_2$ | |
| :-: | :-: |
| **quantite** | **prix** |
| $260$ | $40$ |
| $350$ | $17$ |

</div>

<div style="float:left;margin-right:2em;">

| $T_1 \times T_2$ | | |  |
| :-: | :-: | :-: | :-: |
| **id** | **nom** | **quantite** | **prix** |
| $1$ | Dupont | $260$ | $40$ |
| $1$ | Dupont | $350$ | $17$ |
| $2$ | Dufour | $260$ | $40$ |
| $2$ | Dufour | $350$ | $17$ |
| $3$ | Dumiel | $260$ | $40$ |
| $3$ | Dumiel | $350$ | $17$ |

</div>

</div>

</exp>

<clear></clear>


### Sélection

<def>

Soit $T_1$ une table/entité d'une base de données.
La <bred>Sélection</bred> dans une table $T_1$ est l'opération consistant à ne retenir que les enregistrements de $T_1$ vérifiant une condition donnée :

</def>

<exp data="Sélection dans une Table">

On dispose de la table $T_1$ suivante:

<div style="width:80%; margin:auto;">

<div style="float:left;margin-right:2em;">

| $T_1$ |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **nom** | **prenom** | **genre** |
| $1$ | Dupont  | Gaelle | Femme |
| $2$ | Dupond  | Jean | Homme |
| $3$ | Durand  | Laura | Femme |
| $4$ | Dufour  | Sarah | Femme |
| $5$ | Dumiel  | Paul | Homme |
| $6$ | Durmont  | Karl | Autre |

</div>

<figure style="float:left;margin-right:2em;">

| Sélection dans $T_1$ |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **nom** | **prenom** | **genre** |
| $1$ | Dupont  | Gaelle | Femme |
| $3$ | Durand  | Laura | Femme |
| $4$ | Dufour  | Sarah | Femme |

<figcaption>Résultat de la Requête : <code>genre=Femme</code>

</figcaption>

</figure>

</div>

</exp>

<clear></clear>


### Projection

<def>

Soit $T_1$ une table/entité d'une base de données.
La <bred>Projection</bred> d'une table $T_1$ est l'opération consistant à ne retenir que certains champs/attributs de $T_1$.

</def>

<exp data="Projection d'une Table sur des champs/attributs">

On dispose de la table $T_1$ suivante:

<div style="width:90%; margin:auto;">

<div style="float:left; margin-right:2em;">

| $T_1$ |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **nom** | **prenom** | **genre** |
| $1$ | Dupont  | Gaelle | Femme |
| $2$ | Dupond  | Jean | Homme |
| $3$ | Durand  | Laura | Femme |
| $4$ | Dufour  | Sarah | Femme |
| $5$ | Dumiel  | Paul | Homme |
| $6$ | Durmont  | Karl | Autre |

</div>

<div style="float:left; margin-right:2em;">

| Projection de $T_1$ sur `'id'` et `'prenom'` |  |
| :-: | :-: |
| **id** | **prenom** |
| $1$ | Gaelle |
| $3$ | Laura |
| $4$ | Sarah |

<figcaption>

Projection de $T_1$ sur `'id'` et `'prenom'`

</figcaption>

</div>

</div>

</exp>

<clear></clear>

### Jointures

Une **jointure** est une manière de fusionner virtuellement deux ou plusieurs tables, de manière à ne conserver comme résultats que les enregistrements qui vérifient certaines **conditions caractéristiques** (de la jointure).
Il existe en effet plusieurs types de jointures : C'est la condition choisie qui détermine le type de jointure.

#### Jointure Interne

<def>

Soit $T_1$ et $T_2$ deux tables/entités.
La <bred>jointure interne</bred> de $T_1$ et $T_2$ **sur un champ/attribut donné $A$** est une table formée en ne conservant que les enregistrements de $T_1$ et de $T_2$ ayant **la même valeur commune de $A$**.

</def>

On peut schématiser cette jointure par le schéma suivant:

(Patates à dessiner)

<exp data="Jointure Interne de deux Tables sur un champ/attribut donné">

On dispose des tables $T_1$ et $T_2$ suivantes :

<div style="width:60%; margin:auto;">

<div style="float:left; margin-right:2em;">

| $T_1$ : utilisateur |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **id_ville** | **nom** | **prenom** |
| $1$ | $3$ | Dupont | Gaelle | Femme |
| $2$ | $4$ | Dutronc | Jacques | Homme |
| $3$ | $1$ | Dupond | Jean | Homme |
| $4$ | $3$ | Durand | Laura | Femme |
| $5$ | $1$ | Dufour | Sarah | Femme |
| $6$ | $1$ | Dumiel | Paul | Homme |
| $7$ | $3$ | Durmont | Karl | Autre |

</div>

<div style="float:left; margin-right:2em;">

| $T_2$ : ville |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **nom_ville** |
| $1$ | Marseille |
| $2$ | Toulouse |
| $3$ | Bordeaux |

</div>

</div>

<clear></clear>

<figure style="width:60%; margin:auto;">

| Jointure Interne de $T_1$ et $T_2$ sur `'id_ville'` |  |  |  |  |  |
| :-: | :-: | :-: | :-: | :-: | :-: |
| **id** | **id_ville** | **nom** | **prenom** | **id_ville** | **nom_ville** |
| $1$ | $3$ | Dupont  | Gaelle | $3$ | Bordeaux |
| $3$ | $1$ | Dupond  | Jean | $1$ | Marseille |
| $4$ | $3$ | Durand  | Laura | $3$ | Bordeaux |
| $5$ | $1$ | Dufour  | Sarah | $1$ | Marseille |
| $6$ | $1$ | Dumiel  | Paul | $1$ | Marseille |
| $7$ | $3$ | Durmont  | Karl | $3$ | Bordeaux |

<figcaption>

Jointure Interne de $T_1$ et $T_2$ sur `'id_ville'`

</figcaption>

</figure>

</exp>

<clear></clear>

#### Jointure Externe Gauche

<def data="Jointure Externe Gauche de T1 et T2 sur un attribut A">

Soit $T_1$ et $T_2$ deux tables/entités.
La <bred>jointure externe gauche</bred> de $T_1$ avec $T_2$ **sur un champ/attribut donné $A$** est une table unissant :
* les enregistrements de $T_1$ et de $T_2$ ayant la même valeur commune de $A$ (comme pour une jointure interne), et aussi
* les enregistrements **de la table gauche** $T_1$ pour lesquels la valeur de l'attribut $A$ ne correspond avec aucune valeur de l'attribut $A$ de $T_2$

</def>

On peut schématiser cette jointure par le schéma suivant:

(Patates à dessiner)

<exp data="Jointure Externe Gauche de deux Tables sur un champ/attribut donné">

On dispose des tables $T_1$ et $T_2$ suivantes :

<div style="width:60%; margin:auto;">

<div style="float:left; margin-right:2em;">

| $T_1$ : utilisateur |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **id_ville** | **nom** | **prenom** |
| $1$ | $3$ | Dupont  | Gaelle | Femme |
| $2$ | $4$ | Dutronc | Jacques | Homme |
| $3$ | $1$ | Dupond  | Jean | Homme |
| $4$ | $3$ | Durand  | Laura | Femme |
| $5$ | $1$ | Dufour  | Sarah | Femme |
| $6$ | $1$ | Dumiel  | Paul | Homme |
| $7$ | $3$ | Durmont  | Karl | Autre |

</div>

<div style="float:left; margin-right:2em;">

| $T_2$ : ville |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **nom_ville** |
| $1$ | Marseille |
| $2$ | Toulouse |
| $3$ | Bordeaux |

</div>

</div>

<clear></clear>

<figure style="width:60%; margin:auto;">

| Jointure Externe Gauche de $T_1$ et $T_2$ sur `'id_ville'` |  |  |  |  |  |
| :-: | :-: | :-: | :-: | :-: | :-: |
| **id** | **id_ville** | **nom** | **prenom** | **id_ville** | **nom_ville** |
| $1$ | $3$ | Dupont  | Gaelle | $3$ | Bordeaux |
| $2$ | $4$ | Dutronc | Jacques | NULL | NULL |
| $3$ | $1$ | Dupond  | Jean | $1$ | Marseille |
| $4$ | $3$ | Durand  | Laura | $3$ | Bordeaux |
| $5$ | $1$ | Dufour  | Sarah | $1$ | Marseille |
| $6$ | $1$ | Dumiel  | Paul | $1$ | Marseille |
| $7$ | $3$ | Durmont  | Karl | $3$ | Bordeaux |

<figcaption>

Jointure Externe Gauche de $T_1$ et $T_2$ sur `'id_ville'`

</figcaption>

</figure>

</exp>

<clear></clear>

#### Jointure Externe Droite

<def data="Jointure Externe Droite de T1 et T2 sur un attribut A">

Soit $T_1$ et $T_2$ deux tables/entités.
La <bred>jointure externe droite</bred> de $T_1$ avec $T_2$ **sur un champ/attribut donné $A$** est une table unissant :
* les enregistrements de $T_1$ et de $T_2$ ayant la même valeur commune de $A$ (comme pour une jointure interne), et aussi
* les enregistrements **de la table droite** $T_2$ pour lesquels la valeur de l'attribut $A$ ne correspond avec aucune valeur de l'attribut $A$ de $T_1$

</def>

On peut schématiser cette jointure par le schéma suivant:

(Patates à dessiner)

<exp data="Jointure Externe Droite de deux Tables sur un champ/attribut donné">

On dispose des tables $T_1$ et $T_2$ suivantes :

<div style="width:60%; margin:auto;">

<div style="float:left; margin-right:2em;">

| $T_1$ : utilisateur |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **id_ville** | **nom** | **prenom** |
| $1$ | $3$ | Dupont  | Gaelle | Femme |
| $2$ | $4$ | Dutronc | Jacques | Homme |
| $3$ | $1$ | Dupond  | Jean | Homme |
| $4$ | $3$ | Durand  | Laura | Femme |
| $5$ | $1$ | Dufour  | Sarah | Femme |
| $6$ | $1$ | Dumiel  | Paul | Homme |
| $7$ | $3$ | Durmont  | Karl | Autre |

</div>

<div style="float:left; margin-right:2em;">

| $T_2$ : ville |  |  |  |
| :-: | :-: | :-: | :-: |
| **id** | **nom_ville** |
| $1$ | Marseille |
| $2$ | Toulouse |
| $3$ | Bordeaux |

</div>

</div>

<clear></clear>

<figure style="width:60%; margin:auto;">

| Jointure Externe Droite de $T_1$ et $T_2$ sur `'id_ville'` |  |  |  |  |  |
| :-: | :-: | :-: | :-: | :-: | :-: |
| **id** | **id_ville** | **nom** | **prenom** | **id_ville** | **nom_ville** |
| $1$ | $3$ | Dupont  | Gaelle | $3$ | Bordeaux |
| NULL | NULL | NULL | NULL | $2$ | Toulouse |
| $3$ | $1$ | Dupond  | Jean | $1$ | Marseille |
| $4$ | $3$ | Durand  | Laura | $3$ | Bordeaux |
| $5$ | $1$ | Dufour  | Sarah | $1$ | Marseille |
| $6$ | $1$ | Dumiel  | Paul | $1$ | Marseille |
| $7$ | $3$ | Durmont  | Karl | $3$ | Bordeaux |

<figcaption>

Jointure Externe Droite de $T_1$ et $T_2$ sur `'id_ville'`

</figcaption>

</figure>

</exp>


