# Test Page

## Collapsible Blocks

??? note "Block closed by default"

    The text is inside a collapsible block.

???+ note "Block open by default:"

    The text $\sqrt 2$ is inside a collapsible block.

## drawSVG

```drawsvg
import drawSvg as draw

# Draw a frame of the animation
def draw_frame(t):
    d = draw.Drawing(2, 6.05, origin=(-1,-1.05))
    d.setRenderSize(h=300)
    d.append(draw.Rectangle(-2, -2, 4, 8, fill='white'))
    d.append(draw.Rectangle(-1, -1.05, 2, 0.05, fill='brown'))
    t = (t + 1) % 2 - 1
    y = 4 - t**2 * 4
    d.append(draw.Circle(0, y, 1, fill='lime'))
    return d

with draw.animate_jupyter(draw_frame, delay=0.05) as anim:
# Or:
#with draw.animate_video('example6.gif', draw_frame, duration=0.05
#                       ) as anim:
    # Add each frame to the animation
    for i in range(20):
        anim.draw_frame(i/10)
    for i in range(20):
        anim.draw_frame(i/10)
    for i in range(20):
        anim.draw_frame(i/10)
```


## Géogebra

<iframe src="https://www.geogebra.org/classic/zgwh6kgt?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

## TEST en langage asy

```asy
import math;

size(6cm,0);

add(grid(6,5,lightgray));

pair pA=(1,1), pB=(5,1), pC=(4,4);
 
draw(pA--pB--pC--cycle);
dot("$A$",pA,dir(pC--pA,pB--pA));
dot("$B$",pB,dir(pC--pB,pA--pB));
dot("$C$",pC,dir(pA--pC,pB--pC));
```

# Insertion de graphiques en langages toto

Module de Rodrigo Schwencke, voir  <https://pypi.org/project/mkdocs-markdown-graphviz/> et <https://gitlab.com/rodrigo.schwencke/mkdocs-markdown-graphviz>.

## TEST ASY

```asy
import math;

size(6cm,0);

add(grid(6,5,lightgray));

pair pA=(1,1), pB=(5,1), pC=(4,4);

draw(pA--pB--pC--cycle);
dot("$A$",pA,dir(pC--pA,pB--pA));
dot("$B$",pB,dir(pC--pB,pA--pB));
dot("$C$",pC,dir(pA--pC,pB--pC));
```

???- note "Open Block with toto"

    ```asy tata.png
    import math;

    size(6cm,0);

    add(grid(6,5,lightgray));

    pair pA=(1,1), pB=(5,1), pC=(4,4);

    draw(pA--pB--pC--cycle);
    dot("$A$",pA,dir(pC--pA,pB--pA));
    dot("$B$",pB,dir(pC--pB,pA--pB));
    dot("$C$",pC,dir(pA--pC,pB--pC));
    ```

???+ note "Open Block with toto"

    ```asy toto.svg
    import math;

    size(6cm,0);

    add(grid(6,5,lightgray));

    pair pA=(1,1), pB=(5,1), pC=(4,4);

    draw(pA--pB--pC--cycle);
    dot("$A$",pA,dir(pC--pA,pB--pA));
    dot("$B$",pB,dir(pC--pB,pA--pB));
    dot("$C$",pC,dir(pA--pC,pB--pC));
    ```

## TEST Syntaxe dot

```dot
digraph G {
    rankdir=LR
    TerreTest [peripheries=2]
    Mars
    TerreTest -> Mars
}
```

???+ note "Open Block with toto"

    ```dot
    digraph G {
        rankdir=LR
        TerreTest [peripheries=2]
        Mars
        TerreTest -> Mars
    }
    ```

???- note "Collapsed Block with toto"

    ```dot
    digraph G {
        rankdir=LR
        TerreTest [peripheries=2]
        Mars
        TerreTest -> Mars
    }
    ```

```dot
digraph G {
    rankdir=LR
    TerreTest [peripheries=2]
    Mars
    TerreTest -> Mars
}
```

## TEST Syntaxe graphviz dot test.svg

```graphviz dot un.svg
digraph G {
    rankdir=LR
    TerreGraphTest1 [peripheries=2]
    Mars
    TerreGraphTest1 -> Mars
}
```

???+ note "Open Block with graphviz"

    ```graphviz dot deux.svg
    digraph G {
        rankdir=LR
        TerreGraphTest2 [peripheries=2]
        Mars
        TerreGraphTest2 -> Mars
    }
    ```

???- note "Collapsed Block with graphviz"

    ```graphviz dot trois.svg
    digraph G {
        rankdir=LR
        TerreGraphTest3 [peripheries=2]
        Mars
        TerreGraphTest3 -> Mars
    }
    ```

```graphviz dot quatre.svg
digraph G {
    rankdir=LR
    TerreGraphTest4 [peripheries=2]
    Mars
    TerreGraphTest4 -> Mars
}
```

## Mermaid

???+ note "Block with mermaid"

    ```mermaid
    graph TD
    A[Client] --> B[Load Balancer]
    B --> C[Server01]
    B --> D[Server02]
    B --> E[Server03]
    ```

```mermaid
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
B --> E[Server03]
```

???+ note "Block with mermaid"

    ```mermaid
    graph TD
    A[Client] --> B[Load Balancer]
    B --> C[Server01]
    B --> D[Server02]
    B --> E[Server03]
    ```

```mermaid
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
B --> E[Server03]
```