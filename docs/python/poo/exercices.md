# TNSI : Exercices POO

!!! ex "Encapsulation + Accesseurs / Mutateurs"

    1. Créer une classe Python nommée **CompteBancaire** qui représente un compte bancaire, ayant pour **attributs** :  

        * numeroCompte (type `str`),
        * nom (nom du propriétaire du compte du type `str`), 
        * solde (type `float`)
        On pourra utiliser un **constructeur** `__init__()` ayant comme paramètres
        * `numeroCompte:str` (par défaut numeroCompte="112233"), 
        * `nom:str` (par défaut `nom="DUPOND"`), 
        * `solde:float` (par défaut `solde=0`)
    2. Créer une méthode `depot(montant:float)` qui modélise le dépôt d'un certain `montant` sur le compte :  

        * le `solde` du compte doit être mis à jour
        * une confirmation de la bonne réception de l'argent sur le compte doit apparaître à l'écran (Terminal)  
    3. Créer une méthode `retrait(montant:float)` qui gère les retraits: mise à jour du `solde` du compte $+$ Affichage à l'écran
    4. Créer une méthode `agios()` permettant d'appliquer les **agios** à un pourcentage de 5 % du solde du compte  
    Pour les heureux qui ne connaîtraient pas : les `agios` sont des frais d'ajournement que la banque vous déduit de votre compte, lorsque votre solde est en négatif. 
    5. Modifier la méthode précédente `agios(p:float)` de sorte qu'elle applique les agios à hauteur de `p%` (et non plus forcément 5%)
    6. Créer une méthode `infos()` permettant d’afficher les infos détaillées du compte (numeroCompte+nom+solde)
    7. <enc>**Encapsulation**</enc>  
        **Rappel :** Le concept d'**encapsulation** permet d’éviter une modification par erreur des données/**attributs** d’un objet en bloquant les droits de lecture et de modification des attributs:
        
        * depuis l'extérieur de la classe, pour les attributs **privés** `__attributPrive`
        * depuis l'extérieur de la classe, ou de l'une de ses filles/enfants, pour les attributs **protégés** `_attributPrive`
        Il devient alors nécessaire de passer par ses méthodes qui jouent le rôle d’interface obligatoire: les **accesseurs** et les **mutateurs**.

        Modifier l'attribut `solde` en un attribut **privé**, de sorte que celui-ci ne puisse pas être: ni lu, ni modifié depuis l'extérieur de la classe CompteBancaire. Adapter la classe CompteBancaire pour que celle-ci redevienne fonctionnelle.
    8. <enc>**Accesseurs :fr: / Getters :gb:** et **Mutateurs :fr: / Setters :gb:**</enc>  

        **Rappel :** Pour **lire** et/ou **modifier** des attributs privés et/ou protégés depuis l'extérieur de la classe, on utilise des **méthodes** spécifiquement créées pour cela :

        * les <env>**Accesseurs**</env> :fr: ou <env>**Getters**</env> :gb: sont des **méthodes** qui servent à **lire** la valeur d'un attribut (privé et/ou protégé). Notés usuellement `getXXX()` ou  `get_XXX()`
        * les <env>**Mutateurs**</env> :fr: ou <env>**Setters**</env> :gb: sont des **méthodes** qui servent à **modifier** la valeur d'un attribut (privé et/ou protégé). Notés usuellement `setXXX()` ou  `set_XXX()`

        Écrire la méthode **getter** `getSolde()` qui renvoie la valeur de l'attribut privé `__solde` : cette méthode est appelable depuis l'extérieur de la classe.
        Écrire la méthode **setter** `setSolde(valeur)` qui modifie la valeur de l'attribut privé `__solde` avec la nouvelle `valeur` passée en paramètre: cette méthode est appelable depuis l'extérieur de la classe.

!!! ex "Héritage"

    ```python
    # Définit une classe 'Fille' (=classe Fille/Enfant)
    # qui hérite de la classe 'Mere' (=classe Mère)
    class Fille(Mere):
      pass
    ```

    Dans le code précédent, on dit que la classe `Fille`/`Enfant` **hérite** de la classe `Mere`.  
    En pratique, cela impliquera en particulier que:
    
    * Toute instance de la classe `Fille` hérite également de (donc accède à) :  
        * **tous les attributs** de la classe `Mere`
        * **toutes les méthodes** de la classe `Mere`
    * Il est ***possible*** de <env>**surcharger**</env> les attributs et/ou les méthodes de la classe `Mere`, par ceux de la classe `Fille`/`Enfant`, 
    Cela veut dire que, on peut redéfinir dans la classe `Fille`, les attributs et méthodes de la classe `Mere`  (avec EXACTEMENT le même nom), de sorte à pouvoir les remplacer/écraser/modifier dans un contexte plus spécifique :
        * les **attributs** de la classe `Fille` seront alors utilisés au lieu de ceux de la classe `Mere`, **sur toutes les instances de la classe Fille** :  
        ils sont plus spécifiques pour la classe `Fille`, par rapport à ceux de la classe `Mere`, donc utilisés prioritairement.
        * les **méthodes** de la classe `Fille` seront aussi utilisés au lieu de ceux de la classe `Mere`, **sur toutes les instances de la classe Fille** :  
        elles sont plus spécifiques pour la classe `Fille`, par rapport à ceux de la classe `Mere`, donc utilisés prioritairement.

    On considère le code suivant :

    ```python
    class Personnage:
        # contructeur = méthode __init__ avec 2 arguments (sans compter self !) le nombre de vies et le nom du personnage
        def __init__(self, nbreDeVie, nomDuPerso):
            self.vie = nbreDeVie
            self.nom = nomDuPerso
        # voici la méthode qui affiche le nombre de vies du personnage.
        def affichePointVie(self):
            print('Il reste '+str(self.vie)+' points de vie à '+self.nom)
        # voici la méthode qui fait perdre 1 point de vie au personnage qui a subi une attaque
        def perdVie(self):
            print(self.nom+' subit une attaque, il perd une vie')
            self.vie = self.vie-1
    # la classe Magicien hérite de la classe Personnage
    class Magicien(Personnage):
        # dans def __init__ on retrouve nbreDeVie et nomDuPerso comme dans le def __init__ de la classe Personnage
        def __init__(self, nbreDeVie, nomDuPerso, pointMagie):
            # la ligne suivante est très importante dans le cas d'héritage, il faut systématiquement faire ce genre d'appel :
            # classeparente.__init__(self,arg1,arg2.....) pour hériter de tous les attributs et méthodes de la classe Personnage
            Personnage.__init__(self, nbreDeVie, nomDuPerso)
            # le seul nouvel attribut est self.magie, tous les autres sont hérités de la classe Personnage
            self.magie = pointMagie
        # une méthode uniquement disponible pour les instances de magiciens
        def faireMagie(self):
            print (self.nom+' fait de la magie')
            self.magie = self.magie - 1
            print('Il reste '+str(self.magie)+' points de magie à '+self.nom+'.')
        # les autres méthodes des instances de magicien sont héritées de la classe personnage
    # on crée une instance de Magicien
    gandalf = Magicien(20,'Gandalf',15)
    # applique la méthode affichePointVie à gandalf, cette méthode est héritée de la classe personnage
    gandalf.affichePointVie()
    # applique la méthode faireMagie à gandalf, cette méthode est uniquement applicable aux instances de la classe magicien
    gandalf.faireMagie()
    ```

    1. créer une (autre) instance de la classe `Magicien`, nommée `merlin`, avec `nombreDeVie=30`, `nomDuPerso="Merlin"`, et `pointMagie=17`
    2. afficher les points de vie du magicien `Merlin`
    3. faire perdre 1 point de vie à Merlin
    4. créer une méthode `perdVies(nombre)` de la classe `Personnage` qui:  
        * utilise de manière répétée la méthode `perdVie()` de la même classe `Personnage` (autant de fois que nécessaire)
        * de sorte que le nombre de vies perdues soit égale à `nombre`
    5. créer une méthode `setPointVie(nombre)` (un setter) de la classe `Personnage`, qui modifie le nombre de vies du personnage, de sorte qu'il soit égal à `nombre`
    6. créer une méthode `creeVie()` de la classe `Magicien` qui ajouter 1 vie au personnage
    7. créer une méthode `creeVies(nombre)` de la classe `Magicien` qui:  
        * utilise de manière répétée la méthode `creeVie()` de la même classe `Magicien` (autant de fois que nécessaire)
        * de sorte que le nombre de vies gagnées soit égale à `nombre`
    8. créer une nouvelle classe `Archer` qui hérite de la classe `Personnage`, de sorte qu'elle dispose :  
        * d'un attribut public `nom`, par défaut `"GreenArrow"`
        * d'un attribut public `nbArcs`, par défaut `nbArcs=1`
        * d'un attribut public `nbFleches`, par défaut `nbFleches=10`  
        On pourra créer un constructeur `__init__()` avec ces deux paramètres (et `self`)
    9. instancier la classe `Archer` avec une variable `greenArrow`
    10. Vérifier que l'instance `greenArrow` de la classe Fille `Archer` hérite bien de tous les attributs et méthodes de la classe Mère `Personnage`.
    11. Modifier votre code de sorte à transformer les attributs publics `nbArcs` et `nbFleches`, en des attributs privés.
    créer des méthodes **Getters** et des **Setters** pour `nbArcs`, et pour `nbFleches`.
    12. Créer une méthode `tirerFleche()`, qui :
        * met à jour le compteur du nombre de flèches
        * affiche à l'écran que la flèche a été tirée
    13. Créer une **méthode magique** `__repr__()` qui formatte l'affichage de l'Archer, lors de l'utilisation d'un `print(nom)` ou `nom`
    14. Comment afficher les Points de vie pour `greenArrow`? (Faites-le)
    Enlever des points de Vie à `greenArrow`? (Faites-le)

!!! ex "Quelques Classes de Géométrie"

    <env>**Partie $I$ : Classe Point**</env>

    1. Créer une classe `Point` ayant les **attributs** suivants :  

        * `__x` : un attribut **privé** pour l'**abscisse**, qui est de type `float`
        * `__y` : un attribut **privé** pour l'**ordonnée** qui est de type `float`
        * Par défaut, un Point est initialisé à `O(0;0)`  
        On pourra utiliser un constructeur `__init__()` ayant deux paramètres `x` et `y`
    2. Créer les **getters** `getX()` et `getY()` qui permettent de lire respectivement l'abscisse et l'ordonnée d'un point.  
    Créer les **setters** `setX(self, valeur)` et `setY(self, valeur)` qui permettent de modifier respectivement les abscisses et l'ordonnée d'un point
    3. Créer la méthode magique `__repr__()` qui renvoie la repré́sentation mathématique d’un point dans le Terminal : `(x,y)`.
    4. Créer la méthode ```distance(self, p: `Point`) -> float``` qui renvoie la distance entre le point de l’objet courant (`self`) et l’objet `p` passé en paramètre.  
    Rappel : La distance entre deux points $A(x_A,y_A)$ et $B(x_B,y_B)$, en mathématiques, est égale à :  
    <center><enc>$AB = \sqrt {(x_B−x_A)^2+ (y_B−y_A)^2)}$</enc></center>
    5. Créer la méthode ```milieu(self, p: `Point`) -> `Point` ``` : qui permet de calculer les coordonnées du point `M`, milieu du segment défini par le point de l’objet courant `self` et l’objet Point `p` passé en paramètre. Cette méthode renvoie l'objet Point `M`.  
    Rappel : Les coordonnées du milieu $M(x_M,y_M)$ de $A(x_A,y_A)$ et $B(x_B,y_B)$, sont :  
    <center><enc>$x_M = \dfrac {x_A+x_B}{2} \quad \text{et} \quad y_M = \dfrac {y_A+y_B}{2}$</enc></center>  
    Remarque : La méthode doit renvoyer un objet Point et pas les coordonnées.  

    <env>**Partie $II$ : Classe Droite**</env>  
    On considère une classe Droite disposant des attributs suivants:  

    * `p1` : un attribut **public** de type Point  
    * `p2` : un attribut **public** de type Point  

    <clear></clear>

    1. Créer une méthode `coeff_dir()->float` ou `pente()->float`, qui renvoie en sortie un float : le coefficient directeur `a` de la droite :  
    Rappel : <center><enc>$a = \dfrac {y_B-y_A}{x_B-x_A}$</enc></center>
    2. Créer une méthode `parallele(d1:Droite, d2:Droite)->bool` qui accepte en argument d'entrée deux droites `d1` et `d2`, et renvoie en sortie si elles sont parallèles (`True`), ou pas (`False`)
    3. Créer une méthode `secantes(d1:Droite, d2:Droite)->bool` qui accepte en argument d'entrée deux droites `d1` et `d2`, et renvoie en sortie si elles sont sécantes (`True`), ou pas (`False`)
    4. La droite $(AB)$, passant par les points $A(x_A;y_A)$ et $B(x_B;y_B)$, créer les deux méthodes suivantes:  
        * une méthode `equation_reduite()->str` qui renvoie les deux coefficients `a` et `b` de l'équation réduite `y=ax+b`, donnés par les deux formules suivantes :  
        <center><enc>$a=\dfrac{y_B-y_A}{x_B-x_A} \quad et \quad b=\dfrac{x_By_A-x_Ay_B}{x_B-x_A}$</enc></center>
        * une méthode `affiche_equation_reduite()` qui renvoie une chaîne de caractères affichant l'équation réduite de la droite `y=ax+b` de l'instance courante.  
    5. Créer une méthode `passePar(p:Point)->` qui renvoie un booléen:  
        * `True` si l'instance courante de la droite passe par le point `p` ($\Leftrightarrow$ les coordonnées du point `p` vérifient l'équation de la droite)
        * `False` sinon  

    <env>**Partie III : Classe Triangle**</env>  
    On considére maintenant une classe appelée **Triangle** ayant les **attributs** suivants :  

    * `__p1` : un attribut **privé** de type Point  
    * `__p2` : un attribut **privé** de type Point  
    * `__p3` : un attribut **privé** de type Point  

    <clear></clear>

    1. Écrire les **getters** `getP1()`, `getP2()`, `getP3()`  
    Écrire les **setters** ```setP1(p : `Point`)```, ```setP2(p : `Point`)```, ```setP3(p : `Point`)```
    et un constructeur ```__init__()``` acceptant trois paramètres (et `self`)
    2. Écrire  une  méthode `estPlat(self) -> bool` qui :  
        * renvoie `True` si le triangle est plat (les trois points `p1`, `p2` et `p3` sont alignés)
        * et `False` sinon  

    3. Écrire une méthode `estIsocele(self) -> bool` qui :  
        * renvoie `True` le triangle est Isocèle,
        * `False` sinon.

        Rappel : un triangle $ABC$ est isocèle si $AB = AC$ ou $AB = BC$ ou $BC = AC$
        Remarque : On pourra utiliser une classe `Racine` déjà développée, ou commencer par la créer
    4. Écrire une méthode `estEquilateral(self)->bool` qui renvoie :  
        * `True` si le triangle est équilatéral
        * `False` sinon
    5. Écrire une méthode `estRectangle(self)->bool` qui renvoie :  
        * `True` si le triangle est un triangle Rectangle (Dans ce cas, on précisera en quel point)
        * `False` sinon
    6. Dans un fichier `main.py`, tester toutes les classes et méthodes que vous avez implémentées

!!! ex "Classe Rectangle"

    1. Écrire une classe `Rectangle` en langage Python, permettant de construire un rectangle dotée d'**attributs** `longueur` (par défaut `longeur=10`) et `largeur` (par défaut `largeur=5`).  
    On pourra utiliser un ***constructeur*** `__init__()`  
    2. Créer les **méthodes** suivantes:  

        * une **méthode** `Perimetre()` permettant de calculer le périmètre du rectangle, et  
        * une **méthode** `Aire()` permettant de calculer l'aire du rectangle  
        Rappel: <center><enc>$Aire_{Rectangle}=largeur\times longueur$</enc></center>  
    3. **a.** Créer les getters `getLongueur()` et `getLargeur()` qui permettent de lire (/qui renvoient) la `longueur` et la `largeur` depuis l'extérieur de la classe.  
      **b.** Créer les setters `setLongueur(L:float)` et `setLargeur(l:float)` qui permettent de modifier la `longueur` et la `largeur` depuis l'extérieur de la classe.  
    4. **a.** Créer une classe fille **Parallelogramme** héritant de la classe **Rectangle** telle que :  

        * la classe `Parallelogramme` soit dotée en plus d'un *attribut* **`hauteur`** (par défaut `hauteur=4`)  
        * et d'une autre *méthode* `Aire()` permettant de calculer le volume du Parallélogramme.  
        Rappel : <center><enc>$Aire_{Parallelogramme} = largeur\times hauteur$</enc></center>  

        **b.** Créer une classe fille **Carre** héritant de la classe **Rectangle** telle que :  

          * la classe `Carre` soit dotée en plus d'un *attribut* **`cote`** (par défaut `cote=5`)
          * et d'une autre *méthode* `Aire()` permettant de calculer le volume du Carré.  
          Rappel : <center><enc>$Aire_{Carré} = cote\times cote$</enc></center>  

          * le constructeur `__init__()` fixe par défaut la valeur des attributs :
              * longueur = cote
              * largeur = cote

!!! ex "Classe Cercle"

    1. Définir une classe `Cercle` permettant de créer un cercle $C(O,r)$ de **centre** `O(a,b)` et de **rayon** `r` à l'aide du constructeur `__init__()`
    2. Définir une méthode `Surface()` de la classe qui permet de calculer la surface du cercle
    3. Définir une méthode `Perimetre()` de la classe qui permet de calculer le périmètre du cercle
    4. Définir une méthode `testAppartenance()` de la classe qui permet de tester si un point `A(x,y)` appartient ou non au cercle `C(O,r)`

!!! ex "Arithmétique"

    1. Créer une classe **Calcul** ayant un **constructeur** par défaut (sans paramètres) permettant d’effectuer différents calculs sur les nombres entiers.
    2. Créer au sein de la classe Calcul une méthode nommée Factorielle() qui permet de calculer le factorielle d'un entier. Tester la méthode en faisant une instanciation sur la classe.
    3. Créer au sein de la classe Calcul une méthode nommée Somme() permettant de calculer la somme des n premiers entiers 1+2+3+..+n. Tester la méthode.
    4. Créer au sein de la classe Calcul une méthode nommée testPrim() permettant de tester la primalité d'un entier donné. Tester la méthode.
    5. Créer au sein de la classe Calcul une méthode nommée testPrims() permettant de tester si deux nombres sont premier entre eux.
    6. Créer une méthode tableMult() qui crée et affiche la table de multiplication d'un entier donné. Créer ensuite une méthode `allTablesMult()` permettant d'afficher toutes les tables de multiplications des entiers 1, 2, 3, ..., 9.
    7. Créer une méthode statique `listDiv()` qui récupère tous les diviseurs d'un entier donné sur une liste Ldiv. 8. 8. Créer une autre méthode `listDivPrim()` qui récupère tous les diviseurs premiers d'un entier donné.

!!! ex "Classe Chaîne étendue"

    Coder une classe  myString permettant de doter les chaines de caractères des méthodes append() et pop() faisant les mêmes opérations que celles des listes. Exemple si on crée  des chaines via l'instanciation s1 = myString("Hello") et s2 = "bonjour", et on lui applique les méthodes :

    ```python
    print(s1.append(" world !")) # affiche  'Hello world !'
    print(s2.pop(2))  # affiche 'bojour'
    ```
