# TNSI : TD Classe Fractions

On se donne la classe `Frac` suivante (à compléter) :

```python
class Frac:
  def __init__(self, ...etc...):
    # ... à compléter ...
```

1. Compléter le constructeur `__init__()` de sorte qu'il reçcoie en entrée deux arguments : le numérateur `num` (par défaut `num=1`) et le dénominateur `denom` (par défaut `denom=1`)
2. Créer une méthode magique `__repr__()` qui représente la fraction de l'instance courante `self` dans un Terminal, sous la forme `num/denom`
3. Créer une méthode `pgcd()` **récursive** qui renvoie le **pgcd** (plus grand commun diviseur) du numérateur `num` et du dénominateur `denom`  
<env>Rappel</env> Si $a$ et $b$ sont deux entiers tels que $a\gt b$. Alors :  
    * `pgcd(a,b) = pgcd(b, a-b)`
    * `pgcd(a,0) = a`
4. En déduire une méthode `simp()` qui simplifie la fraction de l'instance courante `self`
5. Créer une méthode magique `__add__()` qui ajoute deux fractions